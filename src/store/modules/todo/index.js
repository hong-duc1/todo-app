import shortid from 'shortid'
import {
  MAKE_DONE,
  MAKE_UNDONE,
  ADD_TODO,
  REMOVE_DONE,
  REMOVE_TODO,
  UPDATE_TODO,
  GET_TODO
} from './mutation-types'
import todoRepo from '../../../db/todo-repo'
const mutations = {
  [MAKE_DONE](state, todoId) {
    let index = state.todos.findIndex(todo => todo.id === todoId)
    state.todos[index].done = true
    todoRepo.updateTodo(state.todos[index])
  },
  [MAKE_UNDONE](state, todoId) {
    let index = state.todos.findIndex(todo => todo.id === todoId)
    state.todos[index].done = false
    todoRepo.updateTodo(state.todos[index])
  },
  [ADD_TODO](state, todoLabel) {
    let todo = { id: shortid.generate(), label: todoLabel, done: false }
    state.todos.push(todo)
    todoRepo.addTodo(todo)
  },
  [REMOVE_DONE](state) {
    state.todos = state.todos.filter(todo => !todo.done)
    todoRepo.deleteDone()
  },
  [REMOVE_TODO](state, todoId) {
    let index = state.todos.findIndex(todo => todo.id === todoId)
    state.todos.splice(index, 1)
    todoRepo.deleteTodo(todoId)
  },
  [UPDATE_TODO](state, todos) {
    state.todos = todos
  }
}

const actions = {
  [GET_TODO]({ commit }) {
    todoRepo.getTodos().then(todos => {
      commit(UPDATE_TODO, todos)
    })
  }
}

const getters = {
  doneTodos: state => {
    return state.todos.filter(todo => todo.done)
  },
  doTodos: state => {
    return state.todos.filter(todo => !todo.done)
  }
}

const state = {
  todos: []
}

export default {
  state,
  actions,
  mutations,
  getters
}
