/* eslint-disable */
function logError(error) {
  console.log(error.code)
  if (error.exception !== '') console.log(error.exception)
}

export default {
  getTodos() {
    return new Promise((res, rej) => {
      NativeStorage.getItem(
        'todo_list',
        todos => {
          res(todos)
        },
        error => {
          logError(error)
          if (error.code === 2) {
            res([])
          } else {
            rej(error)
          }
        }
      )
    })
  },
  setTodos(todos) {
    return new Promise((res, rej) => {
      NativeStorage.setItem(
        'todo_list',
        todos,
        todos => {
          res(todos)
        },
        error => rej(error)
      )
    })
  },
  updateTodo(todo) {
    return this.getTodos().then(todos => {
      let index = todos.findIndex(_todo => _todo.id === todo.id)
      todos[index] = todo
      return this.setTodos(todos)
    })
  },
  deleteTodo(todoId) {
    return this.getTodos().then(todos => {
      let index = todos.findIndex(todo => todoId)
      todos.splice(index, 1)
      return this.setTodos(todos)
    })
  },
  addTodo(todo) {
    return this.getTodos().then(todos => {
      todos.push(todo)
      return this.setTodos(todos)
    })
  },
  deleteDone() {
    return this.getTodos().then(todos => {
      return this.setTodos(todos.filter(todo => !todo.done))
    })
  }
}
